#include <emmintrin.h>
#include <x86intrin.h>
#include <stdio.h>
#include <stdint.h>

#define PAGESIZE (4096U)

// Tableau en mémoire
uint8_t array[10 * PAGESIZE];

int main(int argc, const char **argv)
{
    int junk = 0;
    register uint64_t time1, time2;
    volatile uint8_t *addr;
    int i;
    // Initialiser la mémoire : force le tableau à être alloué
    for (i = 0; i < 10; i++)
        array[i * PAGESIZE] = 1;

    // Vider le tableau du cache
    for (i = 0; i < 10; i++) {
        // TODO Vidage d'une ligne de cache
    }

    // Laisser le temps aux instructions de vidage de cache d'être terminées
    for (volatile int z = 0; z < 100; z++) continue;

    // TODO Accéder à quelques éléments de array
    
    // Mesurer les temps d'accès
    for (i = 0; i < 10; i++)
    {
        addr = &array[i * PAGESIZE];
        // TODO prendre le timestamp CPU avant lecture 

        junk = *addr;
        // TODO calculer le temps écoulé dans time2
        
        printf("Temps d'accès à array[%d * %d]: %ld cycles CPU.\n", i, PAGESIZE, time2);
    }

    return 0;
}
 