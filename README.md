# TD Vulnérabilités par canaux auxiliaires

## Introduction

Dans ce TD on va tenter de réaliser une attaque type 'Spectre' dans un environnement
contrôlé. 

Cette attaque exploite une vulnérabilité matérielle présente dans de nombreux processeurs
et permet de passer outre l'isolation intra et inter processus. Comme il s'agit d'une vulnérabilité
matérielle liée au design et au principe de fonctionnement des processeurs, il est assez difficile de
s'en défendre, même si les fabricants de CPU y travaillent.

En particulier, Spectre permet de faire fuir de l'information entre *sandboxes* dans les applications qui utilisent
ce genre de mécanismes pour isoler leurs composants. Par exemple, les navigateurs web l'utilisent pour isoler les onglets
de navigation les uns des autres.

## Ce qu'on va voir

- Caches CPU
- Exécution spéculative, 'out-of-order' et prédiction de branchement dans les CPU
- Exploitation du cache comme canal auxiliaire
- Vulnérabilité Spectre

## Compilation des codes

Tous les codes devront être compilés comme suit :

```code bash
    gcc -O0 -o <binaire> <fichier.c>
```

Les programmes doivent être ensuite lancés somme suit :

```code bash
    taskset 1 ./<binaire>
```

La commande ```taskset``` permet de définir l'affinité CPU d'un processus. Ici on fait en sorte que le processus soit
toujours exécuté sur le même coeur.

## Utilisation du cache comme canal auxiliaire

Meltdown et Spectre utilisent le cache comme canal auxiliaire pour révéler un secret protégé. La technique utilisée est 
dite FLUSH + RELOAD (vidage et rechargement). On va étudier cette technique en premier.

### Fonctions utiles

- Vider une ligne de cache : `_mm_clflush` : [doc intel](https://software.intel.com/sites/landingpage/IntrinsicsGuide/#text=_mm_clflush)
- Lire le timestamp CPU : `__rdtscp` : [doc intel](https://software.intel.com/sites/landingpage/IntrinsicsGuide/#text=__rdtscp)

### Etape 1 : mesure du temps d'accès au cache

Fichier à compléter / compiler : `cachetime.c`

Complétez `cachetime.c` pour mesurer les différences de temps d'accès entre des données dans le cache et hors cache.
Exécutez le programme une dizaine de fois, en déduire un seuil approximatif qui différencie ces deux temps d'accès.
On se servira de ce seuil pour déterminer si une donnée est dans le cache ou non.

### Etape 2 : le cache comme canal auxiliaire

Fichier à compléter / compiler : `flushreload.c`

L'idée est d'utiliser le temps d'accès au cache pour révéler un secret.
On suppose une fonction `victim` qui utilise le secret comme index pour accéder à un tableau. Ce secret est
supposé inaccessible. La technique qu'on utilise pour révéler le secret manipulé par `victim` est appelée FLUSH+RELOAD.
Le principe est le suivant :

1. Vider complètement le tableau de la mémoire cache (partie FLUSH)
2. Appeler la victime. Cette action provoque le chargement dans le cache d'une ligne qui dépend du secret
3. Recharger le tableau dans le cache et mesurer le temps de rechargement de chaque élément. Si le temps est court, l'élément était déjà dans le cache et c'est cet élément que la victime a utilisé : on connaît alors le secret (partie RELOAD).

- Complétez `flushreload.c`
- Pourquoi pas utiliser simplement `array[256]`, c'est à dire un tableau de 256 octets ?
- Pourquoi utiliser un décalage dans les blocs de mémoire (rôle de `DELTA`) ?
- Est ce que la technique est fiable à 100% ? Pourquoi ?

## Exécution spéculative et prédiction de branchements

Fichier à compléter / compiler : `spectreexperiment.c`

Spectre repose sur l'exécution spéculative et la prédiction de branchement, qui est sont des optimisations importantes utilisée dans tous les CPU modernes
pour maximiser le taux d'utilisation de ses unités de calcul (et donc maximiser les performances d'exécution).

### Etape 3

 1. Exécuter le programme (plusieurs fois). Qu'observez-vous ?
 2. Ligne 89, remplacer par `victim(i+20);`. Que se passe-t-il ? Pourquoi ? Remettez le code initial.
 3. Commentez les lignes 86, 87, 94 et 95 (flush de size). Est-ce que l'attaque fonctionne toujours ? Pourquoi ?
 4. Décommentez les lignes 86, 87, 94 et 95.

## Attaque 'Spectre'

Fichier à compléter / compiler : `spectreattack.c`

Dans ce code, on suppose une fonction `restrictedAccess` qui permet l'accès à un buffer limité pour simuler une sandbox.
Le secret est en  dehors de la sandbox. L'accès au secret n'est possible qu'au travers de la fonction `restrictedAccess`
(c'est à dire qu'on a pas le droit de le lire directement). Le but est d'afficher le secret.

### Etape 4

 1. Exécutez le programme et déterminez si vous êtes capable de déterminer la valeur secrète. Pourquoi le résultat n'est
 pas fiable ? Exécutez le programme un nombre suffisant de fois pour être sûr du secret.

### Etape 5 : amélioration de la fiabilité de l'attaque

 Fichier à compléter / compiler : `spectreattackimproved.c`

 L'idée est d'automatiser la répétition de l'attaque et de maintenir un tableau de scores pour chaque succès de cache.
 C'est une approche statistique. Le secret est supposé être la valeur pour laquelle on a eu le plus de succès de cache.

 1. Exécutez le programme. Qu'observez vous sur la fiabilité de l'attaque ?

### Etape 6 : détermination de tout le secret

Copiez `spectreattackimproved.c` en `spectreallsecret.c`.
Modifiez `spectreallsecret.c` pour qu'il retourne tout le secret (toute la chaîne de caractère).

### Etape 7 : contre-mesure

La fonction `__mm_mfence` [doc intel](https://software.intel.com/sites/landingpage/IntrinsicsGuide/#text=_mm_mfence) permet
d'implémenter une contre-mesure efficace en l'appelant à l'endroit approprié dans `restrictedAccess`.

1. Où faudrait-il introduire cette fonction ?
2. Modifiez `spectreattackimproved.c` en conséquence et vérifiez que la sandbox n'est plus vulnérable à l'attaque.
