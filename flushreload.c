#include <emmintrin.h>
#include <x86intrin.h>
#include <stdio.h>
#include <stdint.h>

// Seuil de temps d'accès considéré comme un succès de cache 
#define CACHE_HIT_THRESHOLD (80)
#define PAGESIZE (4096U)
#define DELTA 1024

uint8_t array[256 * PAGESIZE];

/*
   Code de la victime
*/

volatile int temp;  // volatile pour interdire l'optimisation des accès à temp
char secret = 94;

void victim()
{
    temp = array[secret * 4096 + DELTA];
}

/*
   Code d'analyse
*/

// Partie 'FLUSH'
void flushSideChannel()
{
    int i;
    // Ecrire dans le tableau pour forcer l'allocation en RAM (allocation copy-on-write)
    for (i = 0; i < 256; i++)
        array[i * PAGESIZE + DELTA] = 1;

    // Supprimer le tableau du cache
    for (i = 0; i < 256; i++)
        _mm_clflush(&array[i * PAGESIZE + DELTA]);

    // Laisser le temps aux instructions flush de se terminer
    for (volatile int z = 0; z < 100; z++) continue; 
}

// Partie 'RELOAD'
void reloadSideChannel()
{
    int junk = 0;
    register uint64_t time1, time2;
    volatile uint8_t *addr;
    int i, mix_i;

    for (i = 0; i < 256; i++) {
        /* Mélanger les index pour prévenir le chargement prédictif 
           des lignes de cache suivantes*/
        mix_i = ((i * 167) + 13) & 255;

        /* Calcul de l'adresse d'accès en fonction de mix_i */
        addr = &array[mix_i * PAGESIZE + DELTA];

        /* Enregistrer la valeur du compteur de cycles CPU */
        time1 = __rdtscp(&junk);
        junk = *addr;
        /* Calculer le nombre de cycles écoulés */
        time2 = __rdtscp(&junk) - time1;

        // TODO comment savoir que la donnée est dans le cache ?
        if (/* donnée dans le cache */) {
            printf("array[%d * %d + %d] est dans le cache.\n", mix_i, PAGESIZE, DELTA);
            printf("Le secret = %d.\n", mix_i);
        }
    }
}

int main(int argc, const char **argv)
{
    // TODO opération FLUSH
    victim();
    // TODO opération RELOAD
    return 0;
}
